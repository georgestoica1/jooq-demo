package com.finfabrik.persistence;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
@AllArgsConstructor
@Builder
public class OffersFilterDto {
    private Long id;
    private Short type;
    private BigDecimal price;
    private Date createdTs;
}
