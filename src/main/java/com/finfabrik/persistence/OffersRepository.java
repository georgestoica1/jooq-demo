package com.finfabrik.persistence;

import io.micronaut.data.jdbc.annotation.JdbcRepository;
import io.micronaut.data.jdbc.runtime.JdbcOperations;
import io.micronaut.data.model.Pageable;
import io.micronaut.data.model.query.builder.sql.Dialect;
import io.micronaut.data.repository.CrudRepository;
import org.jooq.impl.DSL;

import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Objects;

@JdbcRepository(dialect = Dialect.H2)
public abstract class OffersRepository implements CrudRepository<Offer, Long> {
    private final JdbcOperations jdbcOperations;

    @Inject
    public OffersRepository(JdbcOperations jdbcOperations) {
        this.jdbcOperations = jdbcOperations;
    }

    @Transactional
    public List<Offer> find(OffersFilterDto filter, Pageable pageable) {
        var queryRunner = DSL.using(jdbcOperations.getConnection())
                .selectFrom(DSL.table("offer"));

        if (Objects.nonNull(filter.getType())) {
            queryRunner.where(DSL.field("type")
                    .equal(DSL.param("type")))
                    .bind("type", filter.getType());
        }

        if (Objects.nonNull(filter.getPrice())) {
            queryRunner.where(DSL.field("price")
                    .equal(DSL.param("price")))
                    .bind("price", filter.getPrice());
        }

        return queryRunner
                .orderBy(DSL.field("id"))
                .seek(pageable.getNumber())
                .limit(pageable.getSize())
                .fetch().into(Offer.class);
    }
}
