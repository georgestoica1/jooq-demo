package com.finfabrik.persistence;

import io.micronaut.data.annotation.DateCreated;
import io.micronaut.data.annotation.DateUpdated;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.time.Instant;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity(name = "offer")
public class Offer {
    @Id
    @GeneratedValue
    private Long id;
    private Short type;
    private BigDecimal price;
    @DateCreated
    private Instant createTs;
    @DateUpdated
    private Instant updateTs;
}
