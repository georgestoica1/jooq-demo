package com.finfabrik.persistence;

import io.micronaut.data.model.Pageable;
import io.micronaut.test.extensions.junit5.annotation.MicronautTest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;

import java.math.BigDecimal;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@MicronautTest
public class OffersRepositoryTests {
    @Inject
    private OffersRepository offersRepository;

    @BeforeEach
    void setUp() {
        offersRepository.deleteAll();

        insertOffer((short) 1, BigDecimal.TEN);
        insertOffer((short) 2, BigDecimal.TEN);
        insertOffer((short) 1, BigDecimal.valueOf(2.33));
        insertOffer((short) 1, BigDecimal.valueOf(10.43));
        insertOffer((short) 2, BigDecimal.valueOf(11));
    }

    @Test
    public void testFindAllOffers() {
        var result = offersRepository.find(
                OffersFilterDto.builder().build(),
                Pageable.UNPAGED);

        assertNotNull(result);
        assertEquals(5, result.size());
    }

    @Test
    public void testFindAllOffersFirstPage() {
        var result = offersRepository.find(
                OffersFilterDto.builder().build(),
                Pageable.from(0, 2));

        assertNotNull(result);
        assertEquals(2, result.size());
    }

    @Test
    public void testFindOffersOfType1() {
        var result = offersRepository.find(
                OffersFilterDto.builder().type((short) 1).build(),
                Pageable.UNPAGED);

        assertNotNull(result);
        assertEquals(3, result.size());
    }

    @Test
    public void testFindOffersWithGivenPrice() {
        var result = offersRepository.find(
                OffersFilterDto.builder().price(BigDecimal.TEN).build(),
                Pageable.UNPAGED);

        assertNotNull(result);
        assertEquals(2, result.size());
    }

    @Test
    public void testFindOffersWithTypeAndPrice() {
        var result = offersRepository.find(
                OffersFilterDto.builder()
                        .type((short) 1)
                        .price(BigDecimal.TEN).build(),
                Pageable.UNPAGED);

        assertNotNull(result);
        assertEquals(1, result.size());
    }

    private void insertOffer(Short type, BigDecimal price) {
        offersRepository.save(Offer.builder()
                .type(Objects.nonNull(type) ? type : -1)
                .price(Objects.nonNull(price) ? price : BigDecimal.ZERO)
                .build());
    }
}
